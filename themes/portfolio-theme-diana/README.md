# Installation

## Usage

The idea with this theme is to serve as a base theme provinding
updates as a git submodule to the [portfolio-hugo-starter](https://github.com/internet4000/portfolio-hugo-starter).

> If you're planning to install it independently as a theme, follow
> the ([Hugo themes
> documentation](https://gohugo.io/themes/installing-and-using-themes/#install-a-single-theme)).


## Admin CMS

To get the URL `/admin` to work, you need to deploy the site using
[Netlify](https://netlify.com).

Then, you will need to **setup Netlify Identity**,

1. Go to [netlify.com](netlify.com)
2. Go to `your project / settings / identity` page.
3. Click on `enable the identity service`.

Then, either add a new user with your email adress, or activate the
Github provider to login with your Github account.

# Development

To run a local development server run `hugo server`.
