const template = document.createElement('template')

template.innerHTML = `
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <style>
     :host([hidden]) { display: none }
     :host {
     }
     .Component {
	 display: flex;
	 overflow-x: scroll;
	 scrollbar-width: none;
	 -ms-overflow-style: none;
	 padding: 1rem;
	 margin-bottom: 2rem;
     }
     .Component::-webkit-scrollbar {
	 height: 0;
	 width: 0;
     }
     .Component figure {
	 padding-left: 1rem;
	 padding-right: 1rem;
     }
     .Component img {
	 max-height: 75vh;
     }
    </style>
    <div class="Component"></div>
    <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
`

class ImageSlideshow extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'close'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
	this.$component = this.shadowRoot.querySelector('.Component')
    }

    connectedCallback() {
	console.log('image-gallery connected')
	this.render()
    }


    render() {
	console.log(this.children)
	Object.entries(this.children).forEach(item => {
	    this.$component.appendChild(item[1])  
	})
    }
}


customElements.define('image-slideshow', ImageSlideshow)

export default ImageSlideshow
