import collections from './collections.js'

export default {
	config: {
		// Skips config.yml.
		// By not skipping, the configs will be merged, with the js-version taking priority.
		load_config_file: false,
		display_url: window.location.origin,

		backend: {
			name: 'gitlab',
			branch: 'master',
			repo: 'publicservices/dianatelares',
			auth_type: 'pkce',
			app_id: '9a8f6159846bb78fd7e16f7bfce9b6a62e2e5e25bfcce15d55a1f6ab4b917b68'
		},

		media_folder: 'static/media/uploads',
		public_folder: 'media/uploads',

		collections
	}
}
