export default {
    label: 'Radio4000 channel slug',
    name: 'radio4000_slug',
    widget: 'string',
    required: false,
    hint: 'In a URL, the `slug` part is this one: radio4000.com/slug . Use the URL that leads to your Radio4000 channel to find the one you want.',
}
