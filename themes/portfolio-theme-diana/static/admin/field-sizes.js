export default {
    label: 'Sizes',
    name: 'sizes',
    required: false,
    widget: 'select',
    multiple: true,
    hint: 'The sizes in which this article is available.',
    options: ['xs', 's', 'm', 'l', 'xl']
}
