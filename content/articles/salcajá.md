+++
title = "Salcajá"
slug = "salcaja"
draft = true
date_release = "2020-04-02"
price = 70
paypal = "ZSS7B7QJAX3MW"
has_stock = false
sold_out = true
collection = "GentryonGuatemala"
image = "/media/uploads/23.jpg"

[[gallery]]
image = "/media/uploads/14.jpg"

[[gallery]]
image = "/media/uploads/20.jpg"

[[gallery]]
image = "/media/uploads/19.jpg"

[[gallery]]
image = "/media/uploads/23.jpg"
+++
Daily square backpack, elaborated with a cotton fabric, original from Salcajá, Guatemala. That fabric combine golden black white and some more colors, hand weaved in a treadle loom; the original fabric is used in the corte (skirt) that the woman use in the region. It has four pockets, 3 auxiliary (one on the top, one the front and another on the side) and a main one. IKK metal zippers with adjustable handles. Handmade in Guatemala, throught [Pixan](https://www.pixan.design/) association.

\    Size: 30 cm width, 30 cm heigh, 11 cm depth

   Volume : 20 L

It is a unique model produced.
