+++
title = "Salcajá Pink"
slug = "salcaja-pink"
draft = true
date_release = "2020-04-02"
price = 70
paypal = "5ZWEAHF2EG64S"
sold_out = true
collection = "GentryonGuatemala"
image = "/media/uploads/1.jpg"

[[gallery]]
image = "/media/uploads/12.jpg"

[[gallery]]
image = "/media/uploads/1.jpg"

[[gallery]]
image = "/media/uploads/2.jpg"

[[gallery]]
image = "/media/uploads/24.jpg"

[[gallery]]
image = "/media/uploads/dscf2047.jpg"

[[gallery]]
image = "/media/uploads/dscf5091.jpg"
+++
Daily square backpack, elaborated with a cotton fabric, original from Salcajá, Guatemala. That fabric combine pink black white and some more colors, hand weaved in a treadle loom; the original fabric is used in the corte (skirt) that the woman use in the region. It has four pockets, 3 auxiliary (one on the top, one the front and another on the side) and a main one. IKK metal zippers with adjustable handles. Handmade in Guatemala, throught [Pixan](https://www.pixan.design/) association

\    Size: 30 cm width, 30 cm heigh, 11 cm depth

\    Volume : 20 L

It is a unique model produced.
