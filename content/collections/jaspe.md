+++
date_release = "2019-11-12"
id = "Jaspe"
image = "/media/uploads/000091010020.jpg"
title = "Jaspe"

[[gallery]]
  image = "/media/uploads/000091010009.jpg"

[[gallery]]
  image = "/media/uploads/000091010012.jpg"

[[gallery]]
  image = "/media/uploads/000091010011.jpg"

[[gallery]]
  image = "/media/uploads/000091010016.jpg"

[[gallery]]
  image = "/media/uploads/000091010017.jpg"

[[gallery]]
  image = "/media/uploads/000091010018.jpg"

[[gallery]]
  image = "/media/uploads/000091010021.jpg"

[[gallery]]
  image = "/media/uploads/000091010002.jpg"

[[gallery]]
  image = "/media/uploads/000091010005.jpg"

[[gallery]]
  image = "/media/uploads/000091010006.jpg"

[[gallery]]
  image = "/media/uploads/000091010023.jpg"

[[gallery]]
  image = "/media/uploads/000091010024.jpg"

[[gallery]]
  image = "/media/uploads/000091010025.jpg"

[[gallery]]
  image = "/media/uploads/000091010022.jpg"

[[gallery]]
  image = "/media/uploads/000091010028.jpg"

[[gallery]]
  image = "/media/uploads/000091010029.jpg"

[[gallery]]
  image = "/media/uploads/000091010027.jpg"

[[gallery]]
  image = "/media/uploads/img_9389-1-.jpg"

+++
Jaspe is the name of a dyeing and weaving textile technique in Guatemala, also known as Ikat. We have named this collection Jaspe, since it is the main fabric used. It is composed of fresh, simple and sustainable clothing for everyday wear. The clothes were designed in San Miguel Sigüila, a village in the Guatemalan highlands, and were later developed in association with Pixan, based in Quetzaltenango. We taught a cut and confection workshop to Margarita Guzman, Elvia Chiché and Vilma Coyoi, from the Palajunoj valley, who then helped us elaborate different designs. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/Lswv5q_b_ms" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The photo report was shot in Nayarit, Mexico by María Magdalena @magdalenapuigserver

If you are interested in any of the designs or any additional information on prices or customization, please do not hesitate to contact us via email. 

Spring/Summer 2019

\><((((º>
