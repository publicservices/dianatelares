+++
title = "About"
layout = "no-title"
menu = "main"
+++
Diana Telares is a limited edition studio which elaborates womenswear collections and accessories that encompass different projects managed by Diana Mendoza Manzano.

Diana Telares’ proposal is linked to travelling, with the intention of discovering, observing and learning the characteristics of fabrics from around the world through a cultural lens focused on people’s identity. Our efforts are centered on creating limited production collections, where the fabric is the pillar on which the whole process revolves, thus elaborating comfortable, feminine and minimalist pieces. We have two fundamental purposes: educate the public to be able to identify and empower the area’s textile culture/industry, and to be able to dedicate ourselves to what we love, our vocation.

All the collections are made with high quality products and special care, collaborating with local associations, applying ethical practices and using sustainable materials.

Our collection is custom made and we believe in the quality and exclusivity of our items. Current production in our workshop is limited to 20 pieces at a time.

Do not hesitate to get in touch! We will be delighted to answer any questions, take your orders, or arrange an appointment.

[dianatelares@gmail.com](mailto:dianatelares+website@gmail.com)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Lswv5q_b_ms" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

\----

Diana Telares.Studio surge de una pasión común: explorar y comprender la identidad de los pueblos mediante los tejidos y sus aplicaciones para vestir. A través de los materiales utilizados, los colores, las formas y los símbolos, cada población local expresa unos valores compartidos que reflejan su relación con el entorno natural, las técnicas artesanales desarrolladas transmitidas de generación en generación y un sentido de la estética común. Las telas expresan, mediante un complejo lenguaje de significados,  una visión propia de la vida que cada pueblo exhibe con orgullo en su manera de vestir.

![](/media/uploads/p1090518.jpg)
