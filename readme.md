# Paypal
- [https://www.paypal.com/buttons/](https://www.paypal.com/buttons)

# id-gentry shop

This website has been created using:

- [hugo](http://gohugo.io/) as a static site generator
- [netlify-cms](http://netlifycms.org/) for Content Management System
  (CMS)

The website is hosted on [netlify](http://netlify.com/), you can
access it here [https://idgentry.netlify.com/](https://dianatelares.com/)

## Usage

For non technical users, the prefered way of editing content is
through the administrative interface (of netlify-cms) that can be found here
[https://idgentry.netlify.com/admin/](https://idgentry.netlify.com/admin/).

To have access there, your user should be invited as a collaborator to
this Github repository, or to this project's *Netlify identify*
service.

Alternatively, to create new content or edit existing one, you can go to this Github
repository, and Github's interface (or git cli), to edit the files in the `/content` folder.

Note that each new file in this folder should have the file extension
`.md`.

## Deployment

### For this instance

Deploying a new version online is automatic.

Netlify takes care of putting live a new version of the site, with the
latest code and content, each time there is a new commit to this
repository. This either through the Github interface, Netlify's cms
administrative interface, or a simple git commit (to this repository).

### For a new copy of this site

To deploy a new instance, copy of this site, the easiest way is to
`clone` this repository on your Github account, and use
[http://netlify.com/](http://netlify.com/) to host it (just as it has
been setup here).

For this you'll need to change a few things on your copy of this repository:

- in `/config.toml`, set the `title` to your site title
- in the `/content` folder, delete all the existing content. **Never
  delete the `index.md` and `_index.md` files**. These are use by hugo
  to know how to generate the pages of your site.

And and setup a few others on Netlify's project configuration.

In the `identity`'settings panel, you'll have to:

- **activate** the *Identity service*
- put *registration preferences* to **invite only** (optional, but you
  should for security)
- in *External providers*, add the provider **Github**.

## Developping


This website is developped using [hugo static site generator](https://gohugo.io/).

It uses the hugo theme
[portfolio-hugo-theme](https://github.com/internet4000/portfolio-hugo-theme).

If you're planning to change the code of this site, you should refer
to both hugo and it's theme documentations.

## Custom domain name

The website is hosted on Netlify, to put a custom domain name you'll
have update the configuration for this project on the Netlify account
it is hosted at.
